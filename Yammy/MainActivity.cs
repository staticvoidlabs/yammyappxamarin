﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Yammy
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Button button = (Button)FindViewById(Resource.Id.button_next);
            Button button2 = FindViewById<Button>(Resource.Id.button_next);

            button.Click += delegate {
                ImageView myControl = FindViewById<ImageView>(Resource.Id.imageView_dishes);
                myControl.SetImageResource(Resource.Drawable.image2);
            };

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void UpdateImageView(object sender, EventArgs eventArgs)
        {
            
            ImageView myControl = FindViewById<ImageView>(Resource.Id.imageView_dishes);

            myControl.SetImageResource(Resource.Drawable.image2);

        }

        public void UpdateImageView(View view)
        {

            ImageView myControl = FindViewById<ImageView>(Resource.Id.imageView_dishes);

            myControl.SetImageResource(Resource.Drawable.image2);

        }

        public void onClickBtn(View v)
        {
            Toast.MakeText(this, "Clicked on Button", ToastLength.Long).Show();
        }

    }
}
